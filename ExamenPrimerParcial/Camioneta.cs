﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPrimerParcial
{
    class Camioneta:Vehiculo, ICalculo
    {
        //Caracteristicas
        public string TipoCabina { get; set; }

        public double MontoDePago;

        
        /*METODO SOBREESCRITO QUE SE HUBIESE UTILIZADO CON CLASES ABSTRACTAS
        public override void CalculoCobrar()
        {
            if (TipoCabina.Equals("Cabina-doble"))
            {
                MontoDePago = 20000;
            }
            else
            {
                MontoDePago = 15000;
            }

            Console.WriteLine("Tipo de Cabina: {0}; Total a pagar: ${1}", TipoCabina, MontoDePago);
        }*/

        //Metodo de interfaz que demuestra el polimorfismo
        public void CalculoPago()
        {
            if (TipoCabina.Equals("Cabina-doble"))
            {
                MontoDePago = 20000;
            }
            else
            {
                MontoDePago = 15000;
            }

            Console.WriteLine("Tipo de Cabina: {0}; Total a pagar: ${1}", TipoCabina, MontoDePago);
        }


    }
}
