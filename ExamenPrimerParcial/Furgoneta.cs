﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPrimerParcial
{
    class Furgoneta:Vehiculo, ICalculo
    {
        //Caracteristicas
        public int NumeroAsientos { get; set; }
        public double PrecioBase { get; set; }
        public int PrecioPorAsiento { get; set; }

        public double PrecioTotalFurgo;

        /*
        METODO SOBREESCRITO QUE SE HUBIESE UTILIZADO CON CLASES ABSTRACTAS
        public override void CalculoCobrar()
        {
            PrecioTotalFurgo = PrecioBase + (NumeroAsientos * PrecioPorAsiento);

            Console.WriteLine("Precio Base: {0}; N.Asientos: {1}; Precio por asiento: {2}; Total a pagar: ${3}", 
                PrecioBase, NumeroAsientos, PrecioPorAsiento, PrecioTotalFurgo);
        }*/

        //Metodo de interfaz que demuestra el polimorfismo
        public void CalculoPago()
        {
            PrecioTotalFurgo = PrecioBase + (NumeroAsientos * PrecioPorAsiento);

            Console.WriteLine("Precio Base: {0}; N.Asientos: {1}; Precio por asiento: {2}; Total a pagar: ${3}",
                PrecioBase, NumeroAsientos, PrecioPorAsiento, PrecioTotalFurgo);
        }



    }
}
