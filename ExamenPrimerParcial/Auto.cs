﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPrimerParcial
{
    class Auto : Vehiculo, ICalculo
    {
        //Caracteristicas
        public string Tipo { get; set; }
        public double PrecioVehiculo { get; set; }

        public double ValorDePago;



        /* METODO SOBREESCRITO QUE SE HUBIESE UTILIZADO CON CLASES ABSTRACTAS
        public override void CalculoCobrar()
        {
            if (Tipo.Equals("Sedan"))
            {
                ValorDePago = PrecioVehiculo;
            }
            else
            {
                ValorDePago = PrecioVehiculo - (PrecioVehiculo * 0.1);
            }

            Console.WriteLine("Tipo de auto: {0}; Total a pagar: ${1}", Tipo, ValorDePago);
            
        }*/

        //Metodo de interfaz que demuestra el polimorfismo
        public void CalculoPago()
        {
            if (Tipo.Equals("Sedan"))
            {
                ValorDePago = PrecioVehiculo;
            }
            else
            {
                ValorDePago = PrecioVehiculo - (PrecioVehiculo * 0.1);
            }

            Console.WriteLine("Tipo de auto: {0}; Total a pagar: ${1}", Tipo, ValorDePago);

        }
    }
}
