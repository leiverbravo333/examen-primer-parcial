﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPrimerParcial
{
    public class Vehiculo
    {
        //Propiedades (Caracteristicas)
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int AnioCreacion { get; set; }

        //Métodos
        public void Mostrar()
        {
            Console.WriteLine("Marca: {0}; Modelo: {1}; Año de Creación: {2}", Marca, Modelo, AnioCreacion);
        }

        /*METODO ABSTRACTO QUE SE HUBIESE UTILIZADO CON CLASES ABSTRACTAS
         * public abstract void CalculoCobrar();*/

    }
}
