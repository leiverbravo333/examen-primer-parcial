﻿using System;
using System.Collections.Generic;

namespace ExamenPrimerParcial
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instancias 
            //Autos
            Auto Hyundai = new Auto();
            Hyundai.Marca = "Hyundai";
            Hyundai.Modelo = "Elantra";
            Hyundai.AnioCreacion = 2018;
            Hyundai.Tipo = "Sedan";
            Hyundai.PrecioVehiculo = 12000;

            Auto Nissan = new Auto();
            Nissan.Marca = "Nissan";
            Nissan.Modelo = "Tiida";
            Nissan.AnioCreacion = 2017;
            Nissan.Tipo = "Hatchback";
            Nissan.PrecioVehiculo = 9000;

            //Camionetas
            Camioneta Toyota = new Camioneta();
            Toyota.Marca = "Toyota";
            Toyota.Modelo = "Hilux";
            Toyota.AnioCreacion = 2018;
            Toyota.TipoCabina = "Cabina-doble";

            Camioneta Chevrolet = new Camioneta();
            Chevrolet.Marca = "Chevrolet";
            Chevrolet.Modelo = "Luv";
            Chevrolet.AnioCreacion = 2000;
            Chevrolet.TipoCabina = "Cabina-simple";

            //Furgoneta
            Furgoneta Chery = new Furgoneta();
            Chery.Marca = "Chery";
            Chery.Modelo = "Practivan";
            Chery.AnioCreacion = 2015;
            Chery.NumeroAsientos = 8;
            Chery.PrecioBase = 15000;
            Chery.PrecioPorAsiento = 800;

            //Lista de vehiculos creados
            List<Vehiculo> ListaDeVehiculos = new List<Vehiculo>();
            ListaDeVehiculos.Add(Hyundai);
            ListaDeVehiculos.Add(Toyota);
            ListaDeVehiculos.Add(Chery);
            ListaDeVehiculos.Add(Nissan);
            ListaDeVehiculos.Add(Chevrolet);

            foreach (ICalculo vehiculo in ListaDeVehiculos)
            {
                vehiculo.Mostrar();
                vehiculo.CalculoPago();
                /*SE HUBIESE UTILIZADO CON CLASES ABSTRACTAS
                 * vehiculo.CalculoCobrar();*/
                Console.WriteLine();
            }
        }
    }
}
